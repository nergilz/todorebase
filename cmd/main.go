package main

import (
	"github.com/spf13/viper"
)

func main() {
	// if err := InitConfigs(); err != nil {
	// 	logrus.Fatalf("Can not read config: %v", err.Error())
	// }

	// repos := repository.NewRepository()
	// service := service.NewService(repos)
	// handler := handler.NewHandler(service)
	// router := handler.InitHandlers()

	// server := new(todoapp.Server)

	// if err := server.Run(viper.GetString("port"), router); err != nil {
	// 	logrus.Fatal("error running server: %v", err.Error())
	// }
}

func InitConfigs() error {
	viper.AddConfigPath("configs") // имя директории
	viper.SetConfigName("config")  // имя файла
	return viper.ReadInConfig()
}

// clean architecture (bob mike)
// слои:
// http requests --> handler --> service (бизнес логика) --> repository (работа с DB)
